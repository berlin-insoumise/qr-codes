A website to generate and store qr-codes to "well-known" pages for berlin-insoumise.

# Development

1. download/clone this project's git repository ()
2. open the `index.html` file in a web browser (internet access needed to load the qr-code dependency)

Alternativelly, the npm module
[server](https://www.npmjs.com/package/serve) can be used to run a
local development server, at the root of the project directory.

## Dependencies

### `webcomponent-qr-code`

Docs: https://github.com/educastellano/qr-code

We're using the `webcomponent-qr-code` npm module (loaded in `index.html` from a CDN), to generate all qr-codes on the pages.

It allows us to use a custom DOM element in the HTML document, such as:

```html
<qr-code
	data="https://actionpopulaire.fr/groupes/3db3e34b-d05e-436a-bc64-de64aa87cd0f/"
	format="png"
	modulesize="10"></qr-code>
```

Formats can also be `svg`, to allow the export of "scalable vector graphic", that can be resized in any dimensions.

## Deployment to a produciton website

The page is hosted on a gitlab (framagit) [pages](https://docs.gitlab.com/ee/user/project/pages/), see the `pages` job in the `.gitlab-ci.yaml` file.
